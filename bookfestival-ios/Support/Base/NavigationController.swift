//
//  NavigationController.swift
//  bookfestival-ios
//
//  Created by Drygan on 1/25/18.
//

import UIKit

class NavigationController: ENSideMenuNavigationController {

    override func viewDidLoad() {
        super.viewDidLoad()
      sideMenu = ENSideMenu(sourceView: self.view,
                            menuViewController: SideMenuTableViewController(),
                            menuPosition: .left)
      
      sideMenu?.menuWidth = 300
      view.bringSubview(toFront: navigationBar)
      
      navigationBar.setBackgroundImage(UIImage(), for: UIBarMetrics.default)
      navigationBar.shadowImage = UIImage()
    }
}

extension NavigationController: ENSideMenuDelegate {
  func sideMenuWillOpen() {
    print("sideMenuWillOpen")
  }
  
  func sideMenuWillClose() {
    print("sideMenuWillClose")
  }
  
  func sideMenuDidClose() {
    print("sideMenuDidClose")
  }
  
  func sideMenuDidOpen() {
    print("sideMenuDidOpen")
  }
  
  func sideMenuShouldOpenSideMenu() -> Bool {
    return true
  }
}


