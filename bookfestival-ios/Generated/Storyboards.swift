// Generated using SwiftGen, by O.Halligon — https://github.com/SwiftGen/SwiftGen

// swiftlint:disable sorted_imports
import Foundation
import UIKit
import bookfestival_ios

// swiftlint:disable superfluous_disable_command
// swiftlint:disable file_length

protocol StoryboardType {
  static var storyboardName: String { get }
}

extension StoryboardType {
  static var storyboard: UIStoryboard {
    let name = self.storyboardName
    return UIStoryboard(name: name, bundle: Bundle(for: BundleToken.self))
  }
}

struct SceneType<T: Any> {
  let storyboard: StoryboardType.Type
  let identifier: String

  func instantiate() -> T {
    let identifier = self.identifier
    guard let controller = storyboard.storyboard.instantiateViewController(withIdentifier: identifier) as? T else {
      fatalError("ViewController '\(identifier)' is not of the expected class \(T.self).")
    }
    return controller
  }
}

struct InitialSceneType<T: Any> {
  let storyboard: StoryboardType.Type

  func instantiate() -> T {
    guard let controller = storyboard.storyboard.instantiateInitialViewController() as? T else {
      fatalError("ViewController is not of the expected class \(T.self).")
    }
    return controller
  }
}

protocol SegueType: RawRepresentable { }

extension UIViewController {
  func perform<S: SegueType>(segue: S, sender: Any? = nil) where S.RawValue == String {
    let identifier = segue.rawValue
    performSegue(withIdentifier: identifier, sender: sender)
  }
}

// swiftlint:disable explicit_type_interface identifier_name line_length type_body_length type_name
enum StoryboardScene {
  enum About: StoryboardType {
    static let storyboardName = "About"

    static let initialScene = InitialSceneType<bookfestival_ios.AboutViewController>(storyboard: About.self)
  }
  enum Favorites: StoryboardType {
    static let storyboardName = "Favorites"

    static let initialScene = InitialSceneType<bookfestival_ios.FavoritesViewController>(storyboard: Favorites.self)
  }
  enum Main: StoryboardType {
    static let storyboardName = "Main"

    static let initialScene = InitialSceneType<bookfestival_ios.NavigationController>(storyboard: Main.self)

    static let programmViewController = SceneType<bookfestival_ios.ProgrammViewController>(storyboard: Main.self, identifier: "ProgrammViewController")
  }
  enum Map: StoryboardType {
    static let storyboardName = "Map"

    static let initialScene = InitialSceneType<bookfestival_ios.MapViewController>(storyboard: Map.self)
  }
  enum Members: StoryboardType {
    static let storyboardName = "Members"

    static let initialScene = InitialSceneType<bookfestival_ios.MembersViewController>(storyboard: Members.self)

    static let programmViewController = SceneType<bookfestival_ios.MembersViewController>(storyboard: Members.self, identifier: "ProgrammViewController")
  }
  enum Projects: StoryboardType {
    static let storyboardName = "Projects"

    static let initialScene = InitialSceneType<bookfestival_ios.ProjectsViewController>(storyboard: Projects.self)
  }
  enum Search: StoryboardType {
    static let storyboardName = "Search"

    static let initialScene = InitialSceneType<bookfestival_ios.SearchViewController>(storyboard: Search.self)

    static let programmViewController = SceneType<bookfestival_ios.SearchViewController>(storyboard: Search.self, identifier: "ProgrammViewController")
  }
}

enum StoryboardSegue {
}
// swiftlint:enable explicit_type_interface identifier_name line_length type_body_length type_name

private final class BundleToken {}
