// Generated using SwiftGen, by O.Halligon — https://github.com/SwiftGen/SwiftGen

#if os(OSX)
  import AppKit.NSFont
  typealias Font = NSFont
#elseif os(iOS) || os(tvOS) || os(watchOS)
  import UIKit.UIFont
  typealias Font = UIFont
#endif

// swiftlint:disable superfluous_disable_command
// swiftlint:disable file_length

struct FontConvertible {
  let name: String
  let family: String
  let path: String

  func font(size: CGFloat) -> Font! {
    return Font(font: self, size: size)
  }

  func register() {
    guard let url = url else { return }
    var errorRef: Unmanaged<CFError>?
    CTFontManagerRegisterFontsForURL(url as CFURL, .process, &errorRef)
  }

  fileprivate var url: URL? {
    let bundle = Bundle(for: BundleToken.self)
    return bundle.url(forResource: path, withExtension: nil)
  }
}

extension Font {
  convenience init!(font: FontConvertible, size: CGFloat) {
    #if os(iOS) || os(tvOS) || os(watchOS)
    if !UIFont.fontNames(forFamilyName: font.family).contains(font.name) {
      font.register()
    }
    #elseif os(OSX)
    if let url = font.url, CTFontManagerGetScopeForURL(url as CFURL) == .none {
      font.register()
    }
    #endif

    self.init(name: font.name, size: size)
  }
}

// swiftlint:disable identifier_name line_length type_body_length
enum FontFamily {
  enum Roboto {
    static let black = FontConvertible(name: "Roboto-Black", family: "Roboto", path: "ROBOTO-BLACK.TTF")
    static let blackItalic = FontConvertible(name: "Roboto-BlackItalic", family: "Roboto", path: "ROBOTO-BLACKITALIC.TTF")
    static let bold = FontConvertible(name: "Roboto-Bold", family: "Roboto", path: "ROBOTO-BOLD.TTF")
    static let boldItalic = FontConvertible(name: "Roboto-BoldItalic", family: "Roboto", path: "ROBOTO-BOLDITALIC.TTF")
    static let italic = FontConvertible(name: "Roboto-Italic", family: "Roboto", path: "ROBOTO-ITALIC.TTF")
    static let light = FontConvertible(name: "Roboto-Light", family: "Roboto", path: "ROBOTO-LIGHT.TTF")
    static let lightItalic = FontConvertible(name: "Roboto-LightItalic", family: "Roboto", path: "ROBOTO-LIGHTITALIC.TTF")
    static let medium = FontConvertible(name: "Roboto-Medium", family: "Roboto", path: "ROBOTO-MEDIUM.TTF")
    static let mediumItalic = FontConvertible(name: "Roboto-MediumItalic", family: "Roboto", path: "ROBOTO-MEDIUMITALIC.TTF")
    static let regular = FontConvertible(name: "Roboto-Regular", family: "Roboto", path: "ROBOTO-REGULAR.TTF")
    static let thin = FontConvertible(name: "Roboto-Thin", family: "Roboto", path: "ROBOTO-THIN.TTF")
    static let thinItalic = FontConvertible(name: "Roboto-ThinItalic", family: "Roboto", path: "ROBOTO-THINITALIC.TTF")
  }
  enum RobotoCondensed {
    static let bold = FontConvertible(name: "RobotoCondensed-Bold", family: "Roboto Condensed", path: "ROBOTOCONDENSED-BOLD.TTF")
    static let boldItalic = FontConvertible(name: "RobotoCondensed-BoldItalic", family: "Roboto Condensed", path: "ROBOTOCONDENSED-BOLDITALIC.TTF")
    static let italic = FontConvertible(name: "RobotoCondensed-Italic", family: "Roboto Condensed", path: "ROBOTOCONDENSED-ITALIC.TTF")
    static let light = FontConvertible(name: "RobotoCondensed-Light", family: "Roboto Condensed", path: "ROBOTOCONDENSED-LIGHT.TTF")
    static let lightItalic = FontConvertible(name: "RobotoCondensed-LightItalic", family: "Roboto Condensed", path: "ROBOTOCONDENSED-LIGHTITALIC.TTF")
    static let regular = FontConvertible(name: "RobotoCondensed-Regular", family: "Roboto Condensed", path: "ROBOTOCONDENSED-REGULAR.TTF")
  }
  enum RobotoMono {
    static let bold = FontConvertible(name: "RobotoMono-Bold", family: "Roboto Mono", path: "ROBOTOMONO-BOLD.TTF")
    static let boldItalic = FontConvertible(name: "RobotoMono-BoldItalic", family: "Roboto Mono", path: "ROBOTOMONO-BOLDITALIC.TTF")
    static let italic = FontConvertible(name: "RobotoMono-Italic", family: "Roboto Mono", path: "ROBOTOMONO-ITALIC.TTF")
    static let light = FontConvertible(name: "RobotoMono-Light", family: "Roboto Mono", path: "ROBOTOMONO-LIGHT.TTF")
    static let lightItalic = FontConvertible(name: "RobotoMono-LightItalic", family: "Roboto Mono", path: "ROBOTOMONO-LIGHTITALIC.TTF")
    static let medium = FontConvertible(name: "RobotoMono-Medium", family: "Roboto Mono", path: "ROBOTOMONO-MEDIUM.TTF")
    static let mediumItalic = FontConvertible(name: "RobotoMono-MediumItalic", family: "Roboto Mono", path: "ROBOTOMONO-MEDIUMITALIC.TTF")
    static let regular = FontConvertible(name: "RobotoMono-Regular", family: "Roboto Mono", path: "ROBOTOMONO-REGULAR.TTF")
    static let thin = FontConvertible(name: "RobotoMono-Thin", family: "Roboto Mono", path: "ROBOTOMONO-THIN.TTF")
    static let thinItalic = FontConvertible(name: "RobotoMono-ThinItalic", family: "Roboto Mono", path: "ROBOTOMONO-THINITALIC.TTF")
  }
  enum RobotoSlab {
    static let bold = FontConvertible(name: "RobotoSlab-Bold", family: "Roboto Slab", path: "ROBOTOSLAB-BOLD.TTF")
    static let light = FontConvertible(name: "RobotoSlab-Light", family: "Roboto Slab", path: "ROBOTOSLAB-LIGHT.TTF")
    static let regular = FontConvertible(name: "RobotoSlab-Regular", family: "Roboto Slab", path: "ROBOTOSLAB-REGULAR.TTF")
    static let thin = FontConvertible(name: "RobotoSlab-Thin", family: "Roboto Slab", path: "ROBOTOSLAB-THIN.TTF")
  }
}
// swiftlint:enable identifier_name line_length type_body_length

private final class BundleToken {}
