//
//  ProgrammViewController.swift
//  bookfestival-ios
//
//  Created by Drygan on 1/25/18.
//

import UIKit

class ProgrammViewController: BaseViewController{
  
  @IBOutlet weak var tableView: UITableView!
  
//  let testData = [(tag: "Привет", color: UIColor.red),
//                  (tag: "Пока", color: UIColor.blue),
//                  (tag: "Профессия", color: UIColor.orange)]
  let testData = [(fromTime:"11-00", toTime: "11-30", title: "Tiaefhgmnsdfkjg  fga dfg afg sdfasd asd asdf asdf asdf asdfa sd fasdfashdfhasd tle", message: "Messageadsfqwebr qwe f qwf sadf sc vxc zv sad fg as asd fasdf asdf asdf asdf dg", tag: [(tag:"Привет", color: UIColor.cyan),
                                                                                                                                                                                                                                                     (tag:"Привет3", color: UIColor.blue)]),
                  (fromTime:"11-30", toTime: "12-00", title: "Title1", message: "Message1", tag: [(tag:"Привет1", color: UIColor.cyan),
                                                                                                  (tag:"Привет2", color: UIColor.red)]),
                  (fromTime:"12-30", toTime: "13-00", title: "Title2", message: "Message2", tag: [(tag:"Привет1", color: UIColor.cyan),
                                                                                                  (tag:"Привет2", color: UIColor.orange)])]
  
  
  
  var date = Date()
  
  override func viewDidLoad() {
    super.viewDidLoad()
    
    tableView.register(Reusable.programmCell)
    tableView.register(Reusable.programmHeaderView)
    tableView.estimatedRowHeight = Reusable.programmCell.class.estHeight
    tableView.rowHeight = UITableViewAutomaticDimension
    self.sideMenuController()?.sideMenu?.delegate = self
  }
  
  @IBAction func menuTapped(_ sender: Any) {
    toggleSideMenuView()
  }
  
  @IBAction func searchPressed(_ sender: Any) {
    
  }
  
  func increaseDate() {
    date = Calendar.current.date(byAdding: .day, value: 1, to: date)!
  }

  func decreaseDate() {
    date = Calendar.current.date(byAdding: .day, value: -1, to: date)!
  }
  
}

extension ProgrammViewController: ENSideMenuDelegate {
  
  func sideMenuWillOpen() {
    
  }
  
  func sideMenuWillClose() {
    
  }
  
  func sideMenuShouldOpenSideMenu() -> Bool {
    return true
  }
  
  func sideMenuDidOpen() {
    
  }
  
  func sideMenuDidClose() {
    
  }
  
}

extension ProgrammViewController: UITableViewDelegate, UITableViewDataSource {
  
  func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
//    return Reusable.programmHeaderView.class.height
    return UITableViewAutomaticDimension
  }
  
  func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
    let header = Reusable.programmHeaderView.class.newHeader
    
    header.forwardTappedClosure = { [weak self] in
      guard let `self` = self else { return }
      self.increaseDate()
      self.tableView.reloadData()
    }
    header.backTappedClosure = { [weak self] in
      guard let `self` = self else { return }
      self.decreaseDate()
      self.tableView.reloadData()
    }
    header.setContent(date)
    return header
  }
  
  public func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
    return 3
  }

  public func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
    let cell = tableView.dequeue(Reusable.programmCell, for: indexPath)
    cell.selectionStyle = .none
    cell.setContent(testData[indexPath.row].fromTime, testData[indexPath.row].toTime, testData[indexPath.row].title, testData[indexPath.row].message, testData[indexPath.row].tag.first, testData[indexPath.row].tag.last)
    return cell
  }
  
  func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
    return UITableViewAutomaticDimension
  }
}


