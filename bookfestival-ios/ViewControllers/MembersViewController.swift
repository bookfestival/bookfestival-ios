//
//  MembersViewController.swift
//  bookfestival-ios
//
//  Created by Drygan on 1/25/18.
//

import UIKit

class MembersViewController: BaseViewController {
  
  @IBOutlet weak var flowLayout: SectionBackgroundFlowLayout!
  @IBOutlet weak var collectionView: UICollectionView!
  
  override func viewDidLoad() {
    super.viewDidLoad()
    
    collectionView.register(Reusable.memberCell)
    collectionView.register(Reusable.memberCollectionViewHeader, kind: .header)
    
    flowLayout.itemSize = CGSize(width: 150, height: 130)

    flowLayout.headerReferenceSize = CGSize(width: collectionView.frame.width, height: 40)
    self.sideMenuController()?.sideMenu?.delegate = self
  }
  
  override func didReceiveMemoryWarning() {
    super.didReceiveMemoryWarning()
    // Dispose of any resources that can be recreated.
  }
  
  override func viewWillAppear(_ animated: Bool) {
    super.viewWillAppear(true)
    
    collectionView.collectionViewLayout.invalidateLayout()
  }
  @IBAction func menuButtonPressed(_ sender: Any) {
    toggleSideMenuView()
  }
  
  @IBAction func searchButtonPressed(_ sender: Any) {
    
  }
  
}

extension MembersViewController: UICollectionViewDelegate, UICollectionViewDataSource {
  
  func collectionView(_ collectionView: UICollectionView, viewForSupplementaryElementOfKind kind: String, at indexPath: IndexPath) -> UICollectionReusableView {
//    let header = Reusable.memberCollectionViewHeader.class.newHeader
    let header = collectionView.dequeue(Reusable.memberCollectionViewHeader, kind: .header, for: indexPath)
    if indexPath.section == 0 {
      header.setContent("СПЕЦИАЛЬНЫЕ ГОСТИ", backgroundColor: .lightGray)
    } else {
      header.setContent("УЧАСТНИКОВ", 3)
    }
    header.frame.size.height = 40
//    collectionView.reloadData()
    return header
  }
  
  func numberOfSections(in collectionView: UICollectionView) -> Int {
    return 2
  }
  
  func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
    if section == 0 {
      return 8
    } else {
      return 8
    }
  }
  
  func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
    if indexPath.section == 0 {
      let cell = collectionView.dequeue(Reusable.memberCell, for: indexPath)
      
      cell.setContent(Asset.member1.image, name: "секция1")
      
      return cell
      
    } else {
      let cell = collectionView.dequeue(Reusable.memberCell, for: indexPath)
      cell.setContent(Asset.member1.image, name: "секция2")
      return cell
    }
  }
  
  func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
    collectionView.deselectItem(at: indexPath, animated: true)
  }
  
  


}

extension MembersViewController: ENSideMenuDelegate {
  
  func sideMenuWillOpen() {
    
  }
  
  func sideMenuWillClose() {
    
  }
  
  func sideMenuShouldOpenSideMenu() -> Bool {
    return true
  }
  
  func sideMenuDidOpen() {
    
  }
  
  func sideMenuDidClose() {
    
  }
  
}
