//
//  SideMenuTableViewController.swift
//  bookfestival-ios
//
//  Created by Drygan on 1/25/18.
//

import UIKit

class SideMenuTableViewController: UITableViewController {
  
  var selectedMenuItem : Int = 0
  
  let menu:[(String?,UIImage?)] = [("ПРОГРАММА",Asset.calendar.image),
                                   ("УЧАСТНИКИ",Asset.person.image),
                                   ("СПЕЦПРОЕКТЫ",UIImage(named: "")),
                                   ("ИЗБРАННОЕ",Asset.favorites.image),
                                   ("КАРТА",Asset.map.image),
                                   ("О ФЕСТИВАЛЕ",Asset.info.image)]
  override func viewDidLoad() {
    super.viewDidLoad()
    tableView.contentInset = UIEdgeInsetsMake(64.0, 0, 0, 0)
    tableView.separatorStyle = .none
    tableView.backgroundColor = UIColor.clear
    tableView.scrollsToTop = false
    tableView.rowHeight = MenuTableViewCell.height
    tableView.register(Reusable.menuCell)
    
  }
  
  override func viewWillAppear(_ animated: Bool) {
    super.viewWillAppear(animated)
    
    tableView.selectRow(at: IndexPath(row: selectedMenuItem, section: 0), animated: false, scrollPosition: .none)
  }
  
  override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
    return 6
  }
  
  override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
    let cell = tableView.dequeue(Reusable.menuCell, for: indexPath)
    
    cell.setContent(menu[indexPath.row].0, menu[indexPath.row].1)
    
    if indexPath.row == 5 {
      cell.separatorView.alpha = 0
    }
    
    return cell
  }
  
  
  
  override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
    
    print("did select row: \(indexPath.row)")
    
    if (indexPath.row == selectedMenuItem) {
      return
    }
    
    selectedMenuItem = indexPath.row
    
    //    Present new view controller
    //    let mainStoryboard: UIStoryboard = UIStoryboard(name: "main",bundle: nil)
    var destViewController : UIViewController
    switch (indexPath.row) {
    case 0:
//      StoryboardScene.Main.initialScene.instantiate()
      destViewController = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "ProgrammViewController")
    case 1:
      destViewController = UIStoryboard(name: "Members", bundle: nil).instantiateInitialViewController()!
    case 2:
      destViewController = UIStoryboard(name: "Projects", bundle: nil).instantiateInitialViewController()!
    case 3:
      destViewController = UIStoryboard(name: "Favorites", bundle: nil).instantiateInitialViewController()!
    case 4:
      destViewController = UIStoryboard(name: "Map", bundle: nil).instantiateInitialViewController()!
    case 5:
      destViewController = UIStoryboard(name: "About", bundle: nil).instantiateInitialViewController()!
    default:
      destViewController = UIStoryboard(name: "Main", bundle: nil).instantiateInitialViewController()!
      //      instantiateViewController(withIdentifier: "MembersViewController")
    }
    sideMenuController()?.setContentViewController(destViewController)
  }
}
