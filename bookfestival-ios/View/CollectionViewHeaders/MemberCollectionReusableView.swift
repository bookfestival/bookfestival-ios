//
//  MemberCollectionReusableView.swift
//  bookfestival-ios
//
//  Created by Drygan on 1/29/18.
//

import UIKit

class MemberCollectionReusableView: UICollectionReusableView {

  @IBOutlet weak var titleLabel: UILabel!
  
  static let height:CGFloat = 40
  static var newHeader: MemberCollectionReusableView = {
    return Bundle.main.loadNibNamed("MemberCollectionReusableView", owner: nil, options: nil)?.first as! MemberCollectionReusableView
  }()
  
  override func awakeFromNib() {
        super.awakeFromNib()
    self.backgroundColor = .clear
  }
  
  func setContent(_ title: String, _ count: Int? = nil, backgroundColor: UIColor? = .clear) {
    if let count = count {
      titleLabel.text = title + " \(count)"
    } else {
      titleLabel.text = title
    }
    self.backgroundColor = backgroundColor
  }
}
