//
//  MemberCollectionViewHeader.swift
//  bookfestival-ios
//
//  Created by Drygan on 1/29/18.
//

import UIKit

class MemberCollectionViewHeader: UIView {

  @IBOutlet private weak var titleLabel: UILabel!

  static let height:CGFloat = 30
  static var newHeader: MemberCollectionViewHeader = {
    return Bundle.main.loadNibNamed("MemberCollectionViewHeader", owner: nil, options: nil)?.first as! MemberCollectionViewHeader
  }()
  
  override func awakeFromNib() {
    super.awakeFromNib()
    self.backgroundColor = .clear
  }
  
  func setContent(_ title: String?, _ count:Int? = nil) {
    if let count = count {
//      titleLabel.text = title + " \(count)"
    } else {
      titleLabel.text = title
    }
  }
}
