//
//  MenuTableViewCell.swift
//  bookfestival-ios
//
//  Created by Drygan on 1/25/18.
//

import UIKit

class MenuTableViewCell: UITableViewCell {
  
  @IBOutlet private weak var titleLabel: UILabel!
  @IBOutlet private weak var imgView: UIImageView!
  @IBOutlet weak var separatorView: UIView!
  
  static let cellId = "MenuTableViewCell_ID"
  static let height:CGFloat = 50
  
  
  override func awakeFromNib() {
    super.awakeFromNib()
    
  }
  
  func setContent(_ title:String?, _ image:UIImage?) {
    self.titleLabel.text = title
    self.imgView.image = image
  }
  
  override func setSelected(_ selected: Bool, animated: Bool) {
    super.setSelected(selected, animated: animated)
    
    if selected {
    self.contentView.backgroundColor = #colorLiteral(red: 1, green: 0.4464504719, blue: 0, alpha: 1)
    } else {
      self.contentView.backgroundColor = .clear
    }
    
  }
  
}
