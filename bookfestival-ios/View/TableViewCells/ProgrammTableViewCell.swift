//
//  ProgrammTableViewCell.swift
//  bookfestival-ios
//
//  Created by Drygan on 1/27/18.
//

import UIKit

class ProgrammTableViewCell: UITableViewCell {
  
//  @IBOutlet weak var layout: UICollectionViewFlowLayout!
//  @IBOutlet weak var collectionView: UICollectionView!
  @IBOutlet private weak var bgView: UIView!
  @IBOutlet private weak var blueBgView: UIView!
  @IBOutlet private weak var fromTime: UILabel!
  @IBOutlet private weak var toTime: UILabel!
  @IBOutlet private weak var leftCircle: UIView!
  
  @IBOutlet private weak var tag1View: UIView!
  @IBOutlet private weak var tag1: UILabel!
  
  @IBOutlet private weak var tag2View: UIView!
  @IBOutlet private weak var tag2: UILabel!
  
  @IBOutlet private weak var title: UILabel!
  
  @IBOutlet private weak var message: UILabel!
  
  @IBOutlet private weak var favoriteButton: UIButton!
  
  var favoriteButtonTappedClosure: ((Bool) -> Void)?
  
  var tags:[(String,UIColor)]?
  
  static let estHeight:CGFloat = 90
  
  override func awakeFromNib() {
    super.awakeFromNib()

    leftCircle.layer.borderWidth = 1
    leftCircle.layer.borderColor = UIColor.black.cgColor
    leftCircle.layer.cornerRadius = self.leftCircle.frame.height / 2

    bgView.layer.cornerRadius = 8
    bgView.layer.borderWidth = 0.5
    bgView.layer.borderColor = UIColor.lightGray.cgColor
    
    blueBgView.layer.borderColor = UIColor.lightGray.cgColor
    blueBgView.layer.borderWidth = 0.5
    
    tag1View.layer.cornerRadius = 8
    tag2View.layer.cornerRadius = 8
    
    favoriteButton.setImage(Asset.favoriteSelected.image, for: .selected)
    favoriteButton.setImage(Asset.favoriteUnselected.image, for: .normal)
  }
  
  func setContent(_ fromTime: String, _ toTime: String, _ title: String, _ message: String,_ tag1: (String, UIColor)?, _ tag2: (String, UIColor)? ) {
    self.fromTime.text = fromTime
    self.toTime.text = toTime
    self.title.text = title
    self.message.text = message
    if let tag1 = tag1 {
      self.tag1.text = tag1.0
      self.tag1View.backgroundColor = tag1.1
    } else {
      self.tag1.text = ""
      self.tag1View.backgroundColor = .clear
    }
    if let tag2 = tag2 {
      self.tag2.text = tag2.0
      self.tag2View.backgroundColor = tag2.1
    } else {
      self.tag2.text = ""
      self.tag2View.backgroundColor = .clear
    }
  }
  
  @IBAction func favoriteButtonTapped(_ sender: Any) {
    
    if favoriteButton.isSelected {
      favoriteButton.isSelected = false
    } else {
      favoriteButton.isSelected = true
    }
    favoriteButtonTappedClosure?(favoriteButton.isSelected)
  }
  
}

