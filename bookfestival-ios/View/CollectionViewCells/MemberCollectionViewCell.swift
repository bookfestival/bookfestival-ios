//
//  TagCollectionViewCell.swift
//  bookfestival-ios
//
//  Created by Drygan on 1/27/18.
//

import UIKit

class MemberCollectionViewCell: UICollectionViewCell {
  
  @IBOutlet private weak var bgView: UIView!
  @IBOutlet private var imgView: UIImageView!
  @IBOutlet private var nameOfMemberLabel: UILabel!
  
  override func awakeFromNib() {
    super.awakeFromNib()
    
//    self.contentView.translatesAutoresizingMaskIntoConstraints = false
//    self.layer.masksToBounds = true
    imgView.layer.cornerRadius = self.imgView.frame.height / 2
    imgView.layer.borderColor = UIColor.white.cgColor
    imgView.layer.borderWidth = 5
    
//    self.layer.cornerRadius = self.frame.size.height / 2
    
  }
  
  func setContent(_ image: UIImage?, name: String?, backgroundColor: UIColor? = .clear) {
    imgView.image = image
    nameOfMemberLabel.text = name
    bgView.backgroundColor = backgroundColor
  }
  
}
