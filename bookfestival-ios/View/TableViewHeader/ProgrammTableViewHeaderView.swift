//
//  programmTableViewCell.swift
//  bookfestival-ios
//
//  Created by Drygan on 1/27/18.
//

import UIKit

class ProgrammTableViewHeaderView: UIView {

  @IBOutlet weak var dateLabel: UILabel!
  @IBOutlet weak var forwardButton: UIButton!
  @IBOutlet weak var backButton: UIButton!
  
  var forwardTappedClosure:(() -> Void)?
  var backTappedClosure:(() -> Void)?
  
  static let height:CGFloat = 50

  static var newHeader: ProgrammTableViewHeaderView = {
    return Bundle.main.loadNibNamed("ProgrammTableViewHeaderView", owner: nil, options: nil)?.first as! ProgrammTableViewHeaderView
  }()
  
  override func awakeFromNib() {
    super.awakeFromNib()

  }
  
  
  
  
  func setContent(_ date:Date) {
  self.dateLabel.text = date.toString()
  }
  
  @IBAction func forwardButtonTapped(_ sender: Any) {
    forwardTappedClosure?()
  }
  
  @IBAction func backButtonTapped(_ sender: Any) {
    backTappedClosure?()
  }
  
}

extension Date {
  func toString() -> String {
    let dateFormatter = DateFormatter()
    
//    dd MMMM YYYY, dddd. in docs confluens
    
    dateFormatter.dateFormat = "dd MMMM yyyy, EEEE"
    return dateFormatter.string(from: self)
  }
}
