//
//  Reusable.swift
//  bookfestival-ios
//
//  Created by Drygan on 1/25/18.
//

import UIKit
import ReusableKit

enum Reusable {
  
  // MARK:- TableViewCells
  static let menuCell = ReusableCell<MenuTableViewCell>(nibName: "MenuTableViewCell")
  static let programmCell = ReusableCell<ProgrammTableViewCell>(nibName: "ProgrammTableViewCell")
  // MARK:- TableViewHeaders
  static let programmHeaderView = ReusableView<ProgrammTableViewHeaderView>(nibName: "ProgrammTableViewHeaderView")
  // MARK:- CollectionViewHeaders
  static let memberCollectionViewHeader = ReusableView<MemberCollectionReusableView>(nibName: "MemberCollectionReusableView")
  // MARK:- CollectionViewCells
  static let memberCell = ReusableCell<MemberCollectionViewCell>(nibName: "MemberCollectionViewCell")
}
